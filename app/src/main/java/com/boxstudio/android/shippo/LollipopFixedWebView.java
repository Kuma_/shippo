package com.boxstudio.android.shippo;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Description here.
 *
 * Created on: 2019-10-24 17:32
 *
 * @author lty <a href="mailto:lty81372860@gmail.com">Contact me.</a>
 */
public class LollipopFixedWebView extends WebView {

  public LollipopFixedWebView(Context context) {
    super(getFixedContext(context));
  }

  public LollipopFixedWebView(Context context, AttributeSet attrs) {
    super(getFixedContext(context), attrs);
  }

  public LollipopFixedWebView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(getFixedContext(context), attrs, defStyleAttr);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public LollipopFixedWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(getFixedContext(context), attrs, defStyleAttr, defStyleRes);
  }

  public LollipopFixedWebView(Context context, AttributeSet attrs, int defStyleAttr, boolean privateBrowsing) {
    super(getFixedContext(context), attrs, defStyleAttr, privateBrowsing);
  }

  private static Context getFixedContext(Context context) {
    // Android Lollipop 5.0 & 5.1
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
      return context.createConfigurationContext(new Configuration());
    } else {
      return context;
    }
  }
}
