package com.boxstudio.android.shippo

import android.app.Activity
import android.content.*
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.*

class MainActivity: Activity() {

    private var mExitTime: Long = 0

    private val targetURL: String = "http://test.lvmaojun.com/mobile"

    private lateinit var mWeb: WebView

    private lateinit var mTvTip: TextView
    private lateinit var mImgStatus: ImageView
    private lateinit var mProgress: ProgressBar

    private lateinit var mLoadingView: LinearLayout

    private val netStatusChangeReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            if (networkInfo != null && networkInfo.isAvailable) {
                if (mWeb != null && !isDestroyed) {
//                    mWeb.reload()
                }
            } else {
                Toast.makeText(context, "oops~ 网络好像开小差啦", Toast.LENGTH_LONG).show()
            }
        }
    }

    companion object {
        const val ERROR_WEB_VIEW_UNAVAILABLE: Int = 100
        const val ERROR_NET_EXCEPTION: Int = 200
        const val ERROR_RESPONSE_UNAVAILABLE: Int = 300
        const val ERROR_RESPONSE_LENGTH_32: Int = 301
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initNetStatusChangeReceiver()
        loadWeb(targetURL)
    }

    private fun initNetStatusChangeReceiver() {
        val intentFilter = IntentFilter()
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        registerReceiver(netStatusChangeReceiver, intentFilter)
    }

    private fun initView() {
        mTvTip = findViewById(R.id.tv_tip)
        mProgress = findViewById(R.id.progress)
        mImgStatus = findViewById(R.id.img_status)
        mLoadingView = findViewById(R.id.ll_loading)

        initWebView()
    }

    @JavascriptInterface
    fun copy(copyStr: String?) {
        if (!TextUtils.isEmpty(copyStr)) {
            val newPlainText = ClipData.newPlainText("text", copyStr)
            val manager: ClipboardManager = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            manager.setPrimaryClip(newPlainText)

            Toast.makeText(this@MainActivity, "已复制到剪贴板", Toast.LENGTH_SHORT).show()
        }
    }

    @JavascriptInterface
    fun openUrl(urlStr: String?) {
        if (!TextUtils.isEmpty(urlStr)) {
            try {
                val intent = Intent()
                intent.data = Uri.parse(urlStr)
                intent.action = Intent.ACTION_VIEW
                startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(this@MainActivity, "不合法的URL路径，请联系管理员", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initWebView() {
        mWeb = try {
            WebView(this)
        } catch (e: Resources.NotFoundException) {
            LollipopFixedWebView(this)
        }
        mWeb.visibility = View.GONE
        val contentView = findViewById<ViewGroup>(R.id.content_view)
        contentView.addView(mWeb, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        mWeb.webChromeClient = WebChromeClient()
        mWeb.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                val url = request.url
                Log.d("MainActivity","WebView loading url: $url")

                val scheme = url.scheme
                if (scheme?.contains("baidu") == true) return true

                view.loadUrl(url.toString())
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (mLoadingView.visibility == View.VISIBLE) {
                    runOnUiThread {
                        mLoadingView.visibility = View.GONE
                        mWeb.visibility = View.VISIBLE
                    }
                }
                super.onPageFinished(view, url)
            }
        }

        val settings = mWeb.settings
        settings.javaScriptEnabled = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.domStorageEnabled = true
        settings.setAppCacheMaxSize(1024 * 1024 * 8)
        val appCachePath = applicationContext.cacheDir.absolutePath
        settings.setAppCachePath(appCachePath)
        settings.allowFileAccess = true
        settings.setAppCacheEnabled(true)
//        settings.userAgentString = settings.userAgentString + "; FBoxAndroidClient/${BuildConfig.VERSION_NAME}"

        val cookieManager = CookieManager.getInstance()
        cookieManager.setAcceptThirdPartyCookies(mWeb, true)
        CookieManager.setAcceptFileSchemeCookies(true)

        mWeb.addJavascriptInterface(this, "androidClient")
    }

    private fun loadWeb(url: String) {
        if (!isDestroyed) {
            mWeb.loadUrl(url)
        } else {
            showError(ERROR_WEB_VIEW_UNAVAILABLE, "加载页面失败，请联系管理员。")
        }
    }

    private fun showLoading(tipStr: String) {
        mTvTip.text = tipStr

        mImgStatus.visibility = View.GONE
        mProgress.visibility = View.VISIBLE
        mWeb.visibility = View.GONE
        mLoadingView.visibility = View.VISIBLE
    }

    private fun showError(errType: Int, errMsg: String) {
        runOnUiThread {
            val showErrorStr = StringBuffer()
            showErrorStr.append(errMsg)
            when (errType) {
                ERROR_RESPONSE_LENGTH_32 -> showErrorStr.append("Response length illegal")
                ERROR_RESPONSE_UNAVAILABLE -> {
                    if (!isDestroyed) {
                        Toast.makeText(this@MainActivity, "正在跳转至官网...", Toast.LENGTH_SHORT).show()
                        mWeb.loadUrl("http://www.lvmaojun.com")
                    }
                    Toast.makeText(this@MainActivity, showErrorStr.append("请检查是否有新版本").toString(), Toast.LENGTH_LONG).show()
                    return@runOnUiThread
                }
            }
            val toString = showErrorStr.toString()
            Toast.makeText(this@MainActivity, toString, Toast.LENGTH_LONG).show()

            mTvTip.text = toString
            mImgStatus.setImageResource(R.drawable.ic_close_24dp)
            mImgStatus.visibility = View.VISIBLE
            mProgress.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        mWeb.removeJavascriptInterface("androidClient")
        unregisterReceiver(netStatusChangeReceiver)
        super.onDestroy()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
            if (mWeb.canGoBack()) {
                mWeb.goBack()
                return true
            } else {
                if (System.currentTimeMillis() - mExitTime > 2000) {
                    Toast.makeText(this@MainActivity, "再按一次退出", Toast.LENGTH_SHORT).show()
                    mExitTime = System.currentTimeMillis()
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }
}